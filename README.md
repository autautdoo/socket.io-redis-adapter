# The library
This library is the clone of [this repo](https://github.com/socketio/socket.io-redis-adapter), version 4.0.1

## Optimizations
Instead of `JSON.stringify` and `JSON.parse`, `sializer` is used to improve (de)serialization perfromance.
Read [this article](https://pouyae.medium.com/sia-an-ultra-fast-serializer-in-pure-javascript-394a5c2166b8) for more info.

Sializer also knows how to (de)serialize mongo ObjectId
